//
//  Alerts.swift
//  ArtistsPresentetion
//
//  Created by Андрей Романюк on 3/4/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import Foundation
import UIKit

final class Alerts {

    static let settingsUrl = "App-Prefs:"
    
    static func presentConnectionAlert() {
        let alert = UIAlertController(title: R.string.localizable.internetConnection(), message: R.string.localizable.problemWithInternetConnection(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: R.string.localizable.cancel(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: R.string.localizable.settings(), style: .default, handler: { (action) in
            if let url = URL(string: settingsUrl) { UIApplication.shared.open(url) }
        }))
        if let window = UIApplication.shared.keyWindow, let topViewController = window.rootViewController {
            topViewController.present(alert, animated: true)
        }
    }
    
    
    static func presentFailedDataLoadingAlert() {
        let alert = UIAlertController(title: R.string.localizable.dataLoading(), message: R.string.localizable.failedToLoadData(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: AppConstant.ok, style: .cancel))
        if let window = UIApplication.shared.keyWindow, let topViewController = window.rootViewController {
            topViewController.present(alert, animated: true)
        }
    }
    
}
