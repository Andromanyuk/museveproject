//
//  FavoritesViewController.swift
//  ArtistsPresentetion
//
//  Created by Андрей Романюк on 2/15/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import CoreData
import UIKit

final class FavoritesViewController: UIViewController {
    
    // MARK: - Vars & Lets
    private let coreDataInstance = CoreDataManager.instance
    private let dataStore = DataStore.shared
    private var removingList = [String]() {
        didSet {
            navigationBarTitle.title = !removingList.isEmpty ?  "\(R.string.localizable.selectedArtists()) \(removingList.count)" : R.string.localizable.selectArtists()
        }
    }
    private var numberOfFavoriteArtists: Int {
        if let section = coreDataInstance.fetchedResultsController.sections?.first {
            return section.numberOfObjects
        } else {
            return 0
        }
    }
    
    // MARK: - Outlets
    @IBOutlet private weak var favoriteCollectionView: UICollectionView!
    @IBOutlet private weak var haveNoFavoritesLabel: UILabel!
    @IBOutlet private weak var editButton: UIBarButtonItem!
    @IBOutlet private weak var cancelButton: UIBarButtonItem!
    @IBOutlet private weak var navigationBarTitle: UINavigationItem!
    @IBOutlet private weak var trashButton: UIBarButtonItem! {
        didSet {
            trashButton.isEnabled = !removingList.isEmpty
        }
    }
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setRightBarButton(editButton, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.setLeftBarButton(nil, animated: true)
        navigationItem.setRightBarButton(editButton, animated: true)
        deselectAllArtists()
        navigationBarTitle.title = R.string.localizable.favorites()
        favoriteCollectionView.allowsMultipleSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        do {
            try coreDataInstance.fetchedResultsController.performFetch()
            favoriteCollectionView.reloadData()
        } catch {
            print(error)
        }
        if numberOfFavoriteArtists == 0 {
            if navigationItem.rightBarButtonItem == editButton {
                editButton.isEnabled = false
            }
            editButton.isEnabled = false
            haveNoFavoritesLabel.isHidden = false
            favoriteCollectionView.isHidden = true
        } else {
            if navigationItem.rightBarButtonItem == editButton {
                editButton.isEnabled = true
            }
            editButton.isEnabled = true
            favoriteCollectionView.isHidden = false
            haveNoFavoritesLabel.isHidden = true
        }
    }
    
    // MARK: - Actions
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        navigationItem.leftBarButtonItem = nil
        navigationItem.setRightBarButton(editButton, animated: true)
        deselectAllArtists()
        favoriteCollectionView.allowsMultipleSelection = false
        navigationBarTitle.title = R.string.localizable.favorites()
    }
    
    @IBAction func trashAction(_ sender: UIBarButtonItem) {
        if favoriteCollectionView.indexPathsForSelectedItems != nil {
            let localizableString = R.string.localizable.self
            let removingArtistString = removingList.count == 1 ? localizableString.artist() : localizableString.artists()
            let alert = UIAlertController(title: localizableString.removing(), message: "\(localizableString.remove()) \(removingList.count) \(removingArtistString)?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: localizableString.remove(), style: .destructive, handler: { [weak self] (action) in
                if let removingList = self?.removingList {
                    for artistName in removingList {
                        self?.coreDataInstance.deleteObject(withName: artistName, forEntity: AppConstant.favoriteArtistEntity, completion: {
                            do {
                                try self?.coreDataInstance.fetchedResultsController.performFetch()
                            } catch {
                                print(error)
                            }
                            if let currentArtist = self?.dataStore.currentFoundArtist, artistName == currentArtist.name {
                                self?.coreDataInstance.artistIsInDataBase = false
                            }
                        })
                    }
                }
                if let selectedIndeces = self?.favoriteCollectionView.indexPathsForSelectedItems {
                    self?.deselectAllArtists()
                    self?.favoriteCollectionView.deleteItems(at: selectedIndeces)
                    self?.coreDataInstance.saveContext()
                }
                self?.configureAfterRemovingFavorites()
            }))
            alert.addAction(UIAlertAction(title: R.string.localizable.cancel(), style: .cancel))
            present(alert, animated: true)
        }
    }
    
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        navigationBarTitle.title = R.string.localizable.selectArtists()
        navigationItem.setLeftBarButton(cancelButton, animated: true)
        navigationItem.setRightBarButton(trashButton, animated: true)
        favoriteCollectionView.allowsMultipleSelection = true
    }
    
    // MARK: - MyTools
    func configureAfterRemovingFavorites() {
        trashButton.isEnabled = false
        navigationItem.leftBarButtonItem = nil
        navigationItem.setRightBarButton(self.editButton, animated: true)
        navigationBarTitle.title = R.string.localizable.favorites()
        favoriteCollectionView.allowsMultipleSelection = false
        if numberOfFavoriteArtists == 0 {
            editButton.isEnabled = false
            haveNoFavoritesLabel.isHidden = false
            favoriteCollectionView.isHidden = true
        }
    }
    
    func deselectAllArtists(){
        if let indeces = favoriteCollectionView.indexPathsForSelectedItems {
            for index in indeces {
                favoriteCollectionView.deselectItem(at: index, animated: true)
            }
        }
        removingList.removeAll()
    }
}

// MARK: - Extension
extension FavoritesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfFavoriteArtists
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.favoriteArtistCell, for: indexPath)!
        let artist = coreDataInstance.fetchedResultsController.object(at: indexPath) as? FavoriteArtist
        cell.configure(by: artist)
        return cell
    }
}

extension FavoritesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell {
            cell.focus(with: .init(scaleX: 0.95, y: AppConstant.cellScale))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell {
            cell.focus(with: .identity)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if favoriteCollectionView.allowsMultipleSelection {
            trashButton.isEnabled = true
            if let artist = coreDataInstance.fetchedResultsController.object(at: indexPath) as? FavoriteArtist, let name = artist.name {
                removingList.append(name)
            }
        } else {
            if let artist = coreDataInstance.fetchedResultsController.object(at: indexPath) as? FavoriteArtist,
                let name = artist.name, let id = artist.id {
                dataStore.setPresentingOnMapArtist(name: name, id: id, upcomingEventsCount: Int(artist.upcomingEventsCount))
                performSegue(withIdentifier: R.segue.favoritesViewController.pushEvents, sender: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if favoriteCollectionView.allowsMultipleSelection {
            if let artist = coreDataInstance.fetchedResultsController.object(at: indexPath) as? FavoriteArtist, let name = artist.name {
                removingList.removeAll { $0 == name }
            }
            trashButton.isEnabled = !removingList.isEmpty
        }
    }
    
}

extension FavoritesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidthAndHeight = (view.frame.size.width - AppConstant.inset) / 2.0
        return CGSize(width: cellWidthAndHeight, height: cellWidthAndHeight)
    }
}
