//
//  StringConstants.swift
//  ArtistsPresentetion
//
//  Created by Андрей Романюк on 2/19/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import Foundation
import UIKit

enum AppConstant {
    // MARK: - Alerts
    static let ok = "Ok"
    // MARK: - Application
    static let appName = "Museve"
    // MARK: - CollectionView
    static let cellScale: CGFloat = 0.95
    static let inset: CGFloat = 18
    // MARK: - Database Entity
    static let favoriteArtistEntity = "FavoriteArtist"
    // MARK: - Map
    static let minSearchCharactersCount = 2
}
