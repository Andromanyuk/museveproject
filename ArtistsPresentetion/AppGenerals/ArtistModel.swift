//
//  ArtistModel.swift
//  ArtistsPresentetion
//
//  Created by Андрей Романюк on 2/18/19.
//  Copyright © 2019 Андрей Романюк. All rights reserved.
//

import Foundation

struct Artist: Codable {
    
    var id: String
    var name: String
    var mbID: String
    var trackerCount: Int
    var upcomingEventsCount: Int
    
    private var url: String
    private var thumbURL: String?
    private var facebookProfileURL: String?
    private var imageURL: String
    
    init() {
        id = ""
        name = ""
        mbID = ""
        trackerCount = 0
        upcomingEventsCount = 0
        url = ""
        imageURL = ""
    }
    
    func getUrl() -> URL? {
        return URL(string: url)
    }
    
    func getThumbUrl() -> URL? {
        if let thumbUrl = thumbURL {
            return URL(string: thumbUrl)
        } else {
            return nil
        }
    }
    
    func getFacebookUrl() -> URL? {
        if let facebookProfileUrl = facebookProfileURL {
            return URL(string: facebookProfileUrl)
        } else {
            return nil
        }
    }
    
    func getImageUrl() -> URL? {
        return URL(string: imageURL)
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name, url
        case thumbURL = "thumb_url"
        case mbID = "mbid"
        case facebookProfileURL = "facebook_page_url"
        case imageURL = "image_url"
        case trackerCount = "tracker_count"
        case upcomingEventsCount = "upcoming_event_count"
    }
}
